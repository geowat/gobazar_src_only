from django.db.models import Q
from django.shortcuts import render
from django.views.generic import ListView
from products_old.models import ProductOld

import bottlenose
from amazon.api import AmazonAPI # library called python-amazon-simple-product-api


class AmazonProduct:
    def __init__(self, region):
        self.titles = []
        self.image_urls = []
        AWS_ACCESS_KEY_ID = 'AKIAIKBBEQH5NTDCMF4A'
        AWS_SECRET_ACCESS_KEY = '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'
        AWS_ASSOCIATE_TAG = 'shoppy0f-21'
        if region == 'US':
            AWS_ASSOCIATE_TAG = 'shoppy08-20'
        elif region == 'DE':
            AWS_ASSOCIATE_TAG = 'shoppy0f-21'
        elif region == 'UK':
            AWS_ASSOCIATE_TAG = 'shoppy0e5-21'
        elif region == 'IT':
            AWS_ASSOCIATE_TAG = 'shoppy0b-21'
        # amazon = bottlenose.Amazon(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_ASSOCIATE_TAG, Region=reg)
        self.amazon_api = AmazonAPI(AWS_ACCESS_KEY_ID,
                            AWS_SECRET_ACCESS_KEY,
                            AWS_ASSOCIATE_TAG,
                            region=region)
    def search(self, keyword, limit_number_of_products):
        product_search = self.amazon_api.search(Keywords=keyword, SearchIndex='All')
        for idx, product in enumerate(product_search):
            self.titles.append(product.title)
            self.image_urls.append(product.large_image_url)
            if idx > limit_number_of_products: # limit number of products, all is giving an error http
                break

class SearchProductView(ListView):
    template_name = "search/view.html"

    def get_context_data(self, *args, **kwargs):
        context = super(SearchProductView, self).get_context_data(*args, **kwargs)
        query = self.request.GET.get('q')
        context['query'] = query
        # SearchQuery.objects.create(query=query)
        return context

    def get_queryset(self, *args, **kwargs):
        request = self.request
        method_dict = request.GET
        query = method_dict.get('q', None) # method_dict['q'] returns error when dict has no matching key
        if query is not None:
            return ProductOld.objects.search(query)
        # return Product.objects.none()

        # trial
        de_amazon = AmazonProduct('DE')
        de_amazon.search("kindle", 10)
        print(de_amazon.titles)
        # trial

        return ProductOld.objects.featured()

        '''
        __contains = field contains this
        __iexact   = field is exactly this
        '''
