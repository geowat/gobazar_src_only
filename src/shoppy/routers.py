from rest_framework import routers
from ebay_products.viewsets import ProductViewSet
router = routers.DefaultRouter()
router.register(r'ebayproduct', ProductViewSet)