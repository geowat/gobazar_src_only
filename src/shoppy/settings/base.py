"""
Django settings for ecommerce project.

Generated by 'django-admin startproject' using Django 1.11.4.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '%le9)yci3^2326(!f5zeo%#xe32i4savjn^_0gj7&j_4v&5(9b'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'shoppyuz@gmail.com' 
EMAIL_HOST_PASSWORD = 'ShopAtAmazonDeutschland!'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'Shoppy Customer Support <support@shoppy.uz>'
BASE_URL = '127.0.0.1:8000'

MANAGERS = (
    ('Kudrat Isabaev', "shoppyuz@gmail.com"),
)

ADMINS = MANAGERS

DEFAULT_ACTIVATION_DAYS = 7 # Email activation number of days, link expired after

# AMAZON API KEYS
AWS_ACCESS_KEY_ID = 'AKIAIKBBEQH5NTDCMF4A'
AWS_SECRET_ACCESS_KEY = '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'

def GET_AWS_ASSOCIATE_TAG(region='UK'):
    if region == 'US':
        AWS_ASSOCIATE_TAG = 'shoppy08-20'
    elif region == 'DE':
        AWS_ASSOCIATE_TAG = 'shoppy0f-21'
    elif region == 'UK':
        AWS_ASSOCIATE_TAG = 'shoppy06f-21' #'shoppy0da-21' #'shoppy0e5-21'
    elif region == 'IT':
        AWS_ASSOCIATE_TAG = 'shoppy0b-21'
    return AWS_ASSOCIATE_TAG

# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'widget_tweaks',

    #our apps
    'accounts',
    'addresses',
    'analytics',
    'billing',
    'carts',
    'ebay_products',
    'templatelib',
    'marketing',
    'orders',
    'products_old',
   'rest_framework',
    'search',
    'tags',
    'amazon_products',
]

AUTH_USER_MODEL = 'accounts.User' #changes the built-in user model to ours
LOGIN_URL = '/login/'
LOGIN_URL_REDIRECT = '/'
LOGOUT_URL = '/logout/'

FORCE_SESSION_TO_ONE = False
FORCE_INACTIVE_USER_ENDSESSION= False

MAILCHIMP_API_KEY = "8bc0cee61f16e21a8b4da816f8259b57-us7"
MAILCHIMP_DATA_CENTER = "us7"
MAILCHIMP_EMAIL_LIST_ID = "5582eb0a5e"


STRIPE_SECRET_KEY = 'sk_test_Wx5EYhsRRkElo9drrAEu1L0Y'
STRIPE_PUB_KEY = 'pk_test_P6tmwMcbkEBsTq4MOXs0KUMb'

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

LOGOUT_REDIRECT_URL = '/login/'
ROOT_URLCONF = 'shoppy.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'shoppy.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static_my_proj"),
]

STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), "static_cdn", "static_root")
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "static_cdn", "media_root")



CORS_REPLACE_HTTPS_REFERER      = False
HOST_SCHEME                     = "http://"
SECURE_PROXY_SSL_HEADER         = None
SECURE_SSL_REDIRECT             = False
SESSION_COOKIE_SECURE           = False
CSRF_COOKIE_SECURE              = False
SECURE_HSTS_SECONDS             = None
SECURE_HSTS_INCLUDE_SUBDOMAINS  = False
SECURE_FRAME_DENY               = False
