from django.contrib.auth import authenticate, login, get_user_model
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.core.mail import send_mail
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils import translation
# forms
from .forms import ContactForm
from ebay_products.views import EbayCategoryProductView

to_email = getattr(settings, 'EMAIL_HOST_USER', 'shoppyuz@gmail.com')

# def set_language(request):
#     language = request.POST.get('language', settings.LANGUAGE_CODE)
#     translation.activate(language)
#     request.session[translation.LANGUAGE_SESSION_KEY] = language

class HomePageCategoryView(EbayCategoryProductView):
    template_name = "home_page.html"

def home_page(request):
    # set_language(request)
    context = {
        "title": _("Hello World!"),
        "content": _("Welcome to the homepage.")
    }
    if request.user.is_authenticated():
        context["premium_content"] = "YEAHHHHHH"
    return render(request, "home_page.html", context)

def about_page(request):
    context = {
        "title": _("About Page!"),
        "content": _("Welcome to the about page.")
    }
    return render(request, "home_page.html", context)

def contact_page(request):
    contact_form = ContactForm(request.POST or None)
    context = {
        "title": _("Contact"),
        "content": _("Welcome to the contact page."),
        "form": contact_form,
    }
    if contact_form.is_valid():
        cd = contact_form.cleaned_data
        print(cd)
        # sender and receiver emails are the same, but sender's email is added into content for starters, but needs changing later
        # maybe also save the contact form in database?
        msg_content = 'FROM:{name} <{email}>\nSUBJECT:{subject}\n\nMESSAGE:\n{msg}'.format(
            email = cd['email'],
            name = cd['fullname'],
            msg = cd['content'],
            subject=cd['subject']
            )
        from_email = to_email
        send_mail(cd['subject'], msg_content, from_email, [to_email])
        if request.is_ajax():
            return JsonResponse({"message": "Thank you for your submission"})

    if contact_form.errors:
        errors = contact_form.errors.as_json()
        if request.is_ajax():
            return HttpResponse(errors, status=400, content_type='application/json')
    # if request.method == "POST":
    #     #print(request.POST)
    #     print(request.POST.get("fullname"))
    #     print(request.POST.get("email"))
    #     print(request.POST.get("content"))
    return render(request, "contact/view.html", context)

def home_page_old(request):
    html_ = """
    <!doctype html>
    <html lang="en">
      <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <title>Hello, world!</title>
      </head>
      <body>
        <div class='text-center'>
          <h1>Hello, world!</h1>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
      </body>
    </html>
    """
    return HttpResponse(html_)
