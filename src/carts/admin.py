from django.contrib import admin

from .models import Cart

class CartAdmin(admin.ModelAdmin):
	list_display = ('id', 'user', 'total', 'updated')
	filter_horizontal = ('products',) # only ManyToMany field
	raw_id_fields = ('user', )

admin.site.register(Cart, CartAdmin)
