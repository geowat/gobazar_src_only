alabaster==0.7.12
Babel==2.6.0
beautifulsoup4==4.7.1
bottlenose==1.1.8
certifi==2018.11.29
chardet==3.0.4
Django==1.11.20
django-widget-tweaks==1.4.5
djangorestframework==3.9.2
docutils==0.14
drf-writable-nested==0.5.1
ebaysdk==2.1.5
googletrans==2.4.0
idna==2.8
imagesize==1.1.0
Jinja2==2.10
lxml==4.3.4
MarkupSafe==1.1.1
packaging==19.0
Pillow==5.3.0
psycopg2==2.7.7
psycopg2-binary==2.7.7
py4j==0.10.7
Pygments==2.3.1
pyparsing==2.3.1
pyspark==2.4.0
python-amazon-simple-product-api==2.2.11
python-dateutil==2.7.3
pytz==2018.5
requests==2.21.0
six==1.11.0
snowballstemmer==1.2.1
soupsieve==1.7.2
Sphinx==1.8.5
sphinxcontrib-websupport==1.1.0
stripe==2.15.0
urllib3==1.24.1
xmltodict==0.12.0
