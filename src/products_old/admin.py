from django.contrib import admin

from .models import ProductOld

class ProductOldAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'slug']
    class Meta:
        model = ProductOld

admin.site.register(ProductOld, ProductOldAdmin)
