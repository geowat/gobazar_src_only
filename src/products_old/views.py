from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from django.views.generic import ListView, DetailView
from django.shortcuts import render, get_object_or_404

from analytics.mixins import ObjectViewedMixin

from carts.models import Cart

from .models import ProductOld


class ProductFeaturedListView(ListView):
    queryset = ProductOld.objects.all().featured()
    template_name = "products_old/list.html"

    # def get_queryset(self, *args, **kwargs):
    #     request = self.request
    #     return Product.objects.featured()

class ProductFeaturedDetailView(ObjectViewedMixin, DetailView):
    template_name = "products_old/featured-detail.html"

    def get_queryset(self, *args, **kwargs):
        request = self.request
        return ProductOld.objects.all().featured()


class UserProductHistoryView(LoginRequiredMixin, ListView):
    template_name = "products_old/user-history.html"
    def get_context_data(self, *args, **kwargs):
        context = super(UserProductHistoryView, self).get_context_data(*args, **kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        context['cart'] = cart_obj
        return context

    def get_queryset(self, *args, **kwargs):
        request = self.request
        views = request.user.objectviewed_set.by_model(ProductOld, model_queryset=False)
        return views


class ProductListView(ListView):
    #queryset = Product.objects.all()
    template_name = "products_old/list.html"

    # def get_context_data(self, *args, **kwargs):
    #     context = super(ProductListView, self).get_context_data(*args, **kwargs)
    #     print(context)
    #     return context

    def get_context_data(self, *args, **kwargs):
        context = super(ProductListView, self).get_context_data(*args, **kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        context['cart'] = cart_obj
        return context

    def get_queryset(self, *args, **kwargs):
        request = self.request
        return ProductOld.objects.all()

def product_list_view(request):
    queryset = ProductOld.objects.all()
    context = {
        "object_list": queryset
    }
    return render(request, "products_old/list.html", context)

class ProductDetailSlugView(ObjectViewedMixin, DetailView):
    queryset = ProductOld.objects.all()
    template_name = "products_old/detail.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProductDetailSlugView, self).get_context_data(*args, **kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        context['cart'] = cart_obj
        return context

    def get_object(self, *args, **kwargs):
        request = self.request
        slug = self.kwargs.get('slug')
        # instance = get_object_or_404(Product, slug=slug, active=True)
        try:
            instance = ProductOld.objects.get(slug=slug, active=True)
        except instance.DoesNotExist:
            raise Http404("Product does not exist.")
        except ProductOld.MultipleObjectsReturned:
            qs = ProductOld.objects.filter(slug=slug, active=True)
            instance = qs.first()
        except:
            raise Http404("Hmm.")

        # object_viewed_signal.send(instance.__class__, instance=instance, request=request)
        return instance

class ProductDetailView(ObjectViewedMixin, DetailView):
    #queryset = Product.objects.all()
    template_name = "products_old/detail.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProductDetailView, self).get_context_data(*args, **kwargs)
        print(context)
        # context['abc'] = 123
        return context
    def get_object(self, *args, **kwargs):
        request = self.request
        pk = self.kwargs.get('pk')
        instance = ProductOld.objects.get_by_id(pk)
        if instance is None:
            raise Http404("Product doesn't exissst!")
        return instance
    # def get_queryset(self, *args, **kwargs):
    #     request = self.request
    #     pk = self.kwargs.get('pk')
    #     return Product.objects.filter(pk=pk)

def product_detail_view(request, pk=None, *args, **kwargs):
    # instance = Product.objects.get(pk=pk, featured=True) #id - added featured for featured objects
    # instance = get_object_or_404(Product, pk=pk, feature=True)
    # try:
    #     instance = Product.objects.get(id=pk)
    # except Product.DoesNotExist:
    #     print('no product here')
    #     raise Http404("Product doesn't exist!!!")
    # except:
    #     print('huh?')
    instance = ProductOld.objects.get_by_id(pk)
    #print(instance)

    if instance is None:
        raise Http404("Product doesn't exist!")

    # print(instance)
    #
    # qs = Product.objects.filter(id=pk)
    # if qs.exists() and qs.count() == 1:
    #     instance = qs.first()
    # else:
    #     raise Http404("Product doesn't exist")


    context = {
        "object": instance
    }
    print(context)
    return render(request, "products_old/detail.html", context)
