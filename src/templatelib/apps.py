from django.apps import AppConfig


class TemplatelibConfig(AppConfig):
    name = 'templatelib'
