
document.querySelector('#header_one').addEventListener('click', hideCategory);
document.querySelector('#header_two').addEventListener('click', hidePrice);
document.querySelector('#header_three').addEventListener('click', hideFeature);
function hideCategory() {
    let x = document.querySelector('#collapse22');
    if (x.style.display === "none") {
        x.style.display = "block";
    } else { x.style.display = "none"; }
}
function hidePrice() {
    let x = document.querySelector('#collapse33');
    if (x.style.display === "none") {
        x.style.display = "block";
    } else { x.style.display = "none"; }
}
function hideFeature() {
    let x = document.querySelector('#collapse44');
    if (x.style.display === "none") {
        x.style.display = "block";
    } else { x.style.display = "none"; }
}
