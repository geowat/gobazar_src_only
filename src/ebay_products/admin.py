from django.contrib import admin
from .models import Product, ShopType, Attribute, AttributeValue

admin.site.register(Product)

admin.site.register(ShopType)
admin.site.register(Attribute)
admin.site.register(AttributeValue)