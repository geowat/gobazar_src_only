from django.apps import AppConfig


class EbayProductsConfig(AppConfig):
    name = 'ebay_products'
