from rest_framework import serializers

from .models import Product, ShopType, Attribute, AttributeValue
# class ProductSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Product
#         fields = '__all__'

# from drf_writable_nested import WritableNestedModelSerializer

class ShopTypeSerializer(serializers.ModelSerializer):

	class Meta:
		model = ShopType
		fields = ('id', 'name', 'website')

		extra_kwargs = {
			'name': {
				'validators': []},
		}

class AttributeSerializer(serializers.ModelSerializer):
	# value = AttributeValueSerializer(many=True)

	class Meta:
		model = Attribute
		fields = '__all__'
		extra_kwargs = {
			'name': {
				'validators': []},
		}

class AttributeValueSerializer(serializers.ModelSerializer):
	attribute = AttributeSerializer()#, allow_null=True)

	class Meta:
		model = AttributeValue
		fields = '__all__'
		# fields = ('pk', 'name', 'value')

	# def create(self, validated_data):
	# 	attribute = validated_data.pop('attribute', None)
	# 	attribute = Attribute.objects.create(**attribute)
	# 	attribute_value = AttributeValue.objects.create(attribute=attribute, **validated_data)
	# 	return attribute_value


class ProductSerializer(serializers.ModelSerializer):
	shop = ShopTypeSerializer()
	attributes = AttributeValueSerializer(many=True)#many=True)

	def run_validators(self, value):
		for validator in self.validators:
			if isinstance(validator, validators.UniqueTogetherValidator):
				self.validators.remove(validator)
		super(ProductSerializer, self).run_validators(value)


	class Meta:
		model = Product
		# exclude = ['timestamp',]
		# fields = '__all__'
		fields = ["id", "title", "asin", "price", "shop", "attributes"]
		extra_kwargs = {
			'shop': {
				'validators': []},
			'attributes': {
				'validators': []},
		}
		depth = 2

	def create(self, validated_data):
		shop = validated_data.pop('shop', None)
		attributes = validated_data.pop('attributes', None)
		shop, _ = ShopType.objects.get_or_create(**shop)
		# try:
			# shop = ShopType.objects.create(**shop)
		# except ValidationError:
			# shop = ShopType.objects.filter(name="Amazon UK").first()
		# shop = ShopType.objects.create(**shop) or None
		# if shop is None:
			# shop = ShopType.objects.filter(name="Amazon UK").first()

		# product, product_created = Product.objects.get_or_create(**validated_data, shop=shop)
		product = Product.objects.create(**validated_data, shop=shop)
		if attributes is not None:
			for attribute in attributes:
				# serializer = AttributeValueSerializer(attributes=attribute)
				# serializer.save()
				attr  = attribute.pop('attribute', None)
				attr, _  = Attribute.objects.get_or_create(**attr)
				attribute_value, _ = AttributeValue.objects.get_or_create(attribute=attr, **attribute)
				product.attributes.add(attribute_value)
			# 	product.attributes.add(**attribute)
		
		return product

