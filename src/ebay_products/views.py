from django.shortcuts import render
from django.conf import settings
from django.views.generic import ListView, DetailView
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.utils import translation
from django.http import JsonResponse
from django.core.serializers.json import DjangoJSONEncoder

from lxml import objectify, etree
from bs4 import BeautifulSoup

from ebaysdk.finding import Connection as finding
from ebaysdk.shopping import Connection as shopping

from googletrans import Translator
import xmltodict, json

ID_APP = 'KudratIs-shoppy-PRD-e2ccf98b2-2346bbb0'
GLOBAL_ID = 'EBAY-GB' # EBAY-DE, EBAY-US, EBAY-GB
api = finding(appid=ID_APP, config_file=None, siteid=GLOBAL_ID) 
api_shopping = shopping(appid=ID_APP, config_file=None, siteid=GLOBAL_ID)

translator = Translator()

def variation_api_view(request, itemid):
    response_var = api_shopping.execute('GetSingleItem', {
        'ItemID': itemid, #'401222389177',
        'IncludeSelector': ['Variations'] # 'Variations', 'ItemSpecifics'
        })
    root_var = objectify.fromstring(response_var.content)
    # print(etree.tostring(root_var))
    try:
        variations_list = root_var.Item.Variations

        variations_dict = [{
            # "sku": str(x.SKU), # kudrat, 12/05/2019 - not all products have SKU
            "quantity": int(x.Quantity),
            "price": float(x.StartPrice),
            "attributes": {str(a.Name): str(a.Value) for a in x.VariationSpecifics.NameValueList}
            }
            for x in variations_list.Variation[:]]

        # MPN attribute is deleted for now - it may be re-added to process in backend rather than frontend
        for v in variations_dict:
            v['attributes'].pop('MPN', None)

    except AttributeError:
        variations_dict = None

    try:
        pictures_list = root_var.Item.Variations.Pictures
        pictures_dict = [{
            "name": str(pictures_list.VariationSpecificName),
            "value": str(x.VariationSpecificValue),
            "url": [str(url) for url in x.PictureURL[:]]
            }
            for x in pictures_list.VariationSpecificPictureSet[:]]

    except AttributeError:
        pictures_dict = None

    try:
        title = str(root_var.Item.Title)
    except AttributeError:
        title = None

    try:
        main_picture_url = [str(pic) for pic in root_var.Item.PictureURL[:]]
    except AttributeError:
        main_picture_url = None

    try:
        converted_current_price = str(root_var.Item.ConvertedCurrentPrice)
    except AttributeError:
        converted_current_price = None



    variation_data = {
        'itemid': itemid,
        'title': title,
        'converted_current_price': converted_current_price,
        'main_pictures': main_picture_url,
        'variations': variations_dict,
        'variation_pictures': pictures_dict
    }
    # variation_data = {'variations': ['hello', 'bye'], 'pictures': ['1', '2']}
    return JsonResponse(variation_data)


def get_category_products(category_id, entries_per_page):
    response_cat = api.execute('findItemsByCategory', {
    'categoryId': category_id, # 2984 - Baby, 58058 - Computers, 67588 - Health Care, 
    'paginationInput': {'entriesPerPage': entries_per_page}
    })

    root_cat = objectify.fromstring(response_cat.content) # print(response_cat.content)
    result_cat = root_cat.searchResult.item
    return result_cat

class EbayCategoryProductView(ListView):
    # template_name = "templates/home_page.html"
    # this class is called in shoppy.views.py to display in home_page and template_name was assigned there.

    def get_queryset(self, *args, **kwargs):
        pass
        # result_cat = get_category_products('26395')
        # return result_cat

    def get_context_data(self, *args, **kwargs):
        context = super(EbayCategoryProductView, self).get_context_data(*args, **kwargs)
        result_cat_computers = get_category_products('58058', '8')
        result_cat_health    = get_category_products('67588', '8')
        result_cat_baby      = get_category_products('19006', '8')
        context['category_computers'] = result_cat_computers
        context['category_health'] = result_cat_health
        context['category_baby'] = result_cat_baby

        result_cat_recomm = get_category_products('111422', '12') # currently random test category 'Apple laptops' for recommendation
        context['category_recommended'] = result_cat_recomm

        return context

class EbayProductView(ListView):
    template_name = "ebay_products/view.html"

    def get_queryset(self, *args, **kwargs):
        # language = self.request.session[translation.LANGUAGE_SESSION_KEY]

        query = self.request.GET.get('q')
 
        # if language != 'en':
        #     query = translator.translate(query, src=language, dest='en').text

        api_request = {
            'keywords': query,
            'itemFilter': [
                {'name': 'Condition',
                 'value': 'New'},
                {'name': 'LocatedIn',
                 'value': 'GB'}
            ],
            # 'outputSelector': 'GalleryInfo'  # 'galleryPlusPictureURL'
        }


        # response = api.execute('findItemsByKeywords', {'keywords': query})
        response = api.execute('findItemsAdvanced', api_request)
        root_obj = objectify.fromstring(response.content)

        result = root_obj.searchResult.item
        ## for x in result:
        ##     print(etree.tostring(x.title))
        return result


class EbayProductDetailView(DetailView):
    template_name = "ebay_products/vue_detail.html"

    # def get_queryset(self, *args, **kwargs):
    #     pass

    def get_object(self, *args, **kwargs):
        itemid = self.kwargs.get('itemid')

        response_var = api_shopping.execute('GetSingleItem', {
            'ItemID': itemid,
            'IncludeSelector': ['Variations'] # 'Variations', 'ItemSpecifics'
            })

        # print(response_var.content)

        response = api_shopping.execute('GetSingleItem', {
            'ItemID': itemid,
            'IncludeSelector': ['ItemSpecifics'] # 'Variations', 'ItemSpecifics'
            })

        response_desc = api_shopping.execute('GetSingleItem', {
            'ItemID': itemid,
            'IncludeSelector': ['Description'] # 'Variations', 'ItemSpecifics'
            })
        
        root_obj = objectify.fromstring(response.content)
        root_desc = objectify.fromstring(response_desc.content)

        result = root_obj.Item # GetSingleItemResponse
        result.Description = root_desc.Item.Description       

        # print(response_var.content)
        root_var = objectify.fromstring(response_var.content)
        # result.variations_list = etree.tostring(root_var.Item.Variations)
        try:
            # variations_list = json.dumps(xmltodict.parse(etree.tostring(root_var.Item.Variations)))
            variations_list = root_var.Item.Variations
            result.variations_list = variations_list
            result.attributes_list = root_var.Item.Variations.VariationSpecificsSet.NameValueList[:]

            variations_dict = [{
                "sku": x.SKU,
                "quantity": x.Quantity,
                "price": x.StartPrice,
                "attributes": [{a.Name: a.Value for a in x.VariationSpecifics.NameValueList}]
                }
                for x in variations_list.Variation[:]]
        except AttributeError:
            pass


        try:
            pictures_list = root_var.Item.Variations.Pictures
            pictures_dict = [{
                "name": pictures_list.VariationSpecificName,
                "value": x.VariationSpecificValue,
                "url": [{url for url in x.PictureURL[:]}]
                }
                for x in pictures_list.VariationSpecificPictureSet[:]]
        except AttributeError:
            pass

        return result

    # def get_context_data(self, *args, **kwargs):
    #     # obj = super(EbayProductDetailView, self).get_object(queryset)
    #     itemid = self.kwargs.get('itemid')
    #     response_var = api_shopping.execute('GetSingleItem', {
    #         'ItemID': itemid,
    #         'IncludeSelector': ['Variations'] # 'Variations', 'ItemSpecifics'
    #         })
    #     root_var = objectify.fromstring(response_var.content)
    #     context = super(EbayProductDetailView, self).get_context_data(*args, **kwargs)
    #     # context = {}
    #     context["variations_json"] = json.dumps(xmltodict.parse(etree.tostring(root_var.Item.Variations.Variation[:])))
    #     return context