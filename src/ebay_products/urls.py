from django.conf.urls import url

from .views import (
        EbayProductView,
        EbayProductDetailView,
        variation_api_view,
        )

urlpatterns = [
    url(r'^$', EbayProductView.as_view(), name='query'),
    url(r'^api/(?P<itemid>[\w-]+)/$', variation_api_view, name='api-variation'),
    url(r'^(?P<itemid>[\w-]+)/$', EbayProductDetailView.as_view(), name='detail'),

]