from django.db import models

class ShopType(models.Model):
	name        = models.CharField(max_length=200, unique=True)
	website     = models.URLField()

	def __str__(self):
		return self.name

	# class Meta:
	# 	unique_together = ('name', 'website',)

class Attribute(models.Model):
	name        = models.CharField(max_length=200, unique=True)
	description = models.CharField(max_length=250, blank=True)

	def __str__(self):
		return self.name

class AttributeValue(models.Model):
	attribute   = models.ForeignKey(Attribute, on_delete=models.CASCADE)
	value       = models.CharField(max_length=250)

	def __str__(self):
		return "{}, {}".format(self.attribute.name, self.value)

class Product(models.Model):
    title       = models.CharField(max_length=120)
    asin        = models.CharField(blank=True, max_length=50, unique=False)
    # shop        = models.CharField(max_length=120, blank=True)
    shop        = models.ForeignKey(ShopType, blank=True, null=True, on_delete=models.SET_NULL)
    description = models.TextField(blank=True)
    price       = models.DecimalField(decimal_places=2, max_digits=20, default=39.99)
    attributes  = models.ManyToManyField(AttributeValue, blank=True)
    timestamp   = models.DateTimeField(auto_now_add=True)


    def __str__(self):
    	return self.title