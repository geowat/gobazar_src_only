from django.conf import settings
import bottlenose
from lxml import objectify, etree

from bs4 import BeautifulSoup
from amazon.api import AmazonAPI # library called python-amazon-simple-product-api

region = 'UK'

AWS_ACCESS_KEY_ID = getattr(settings, 'AWS_ACCESS_KEY_ID', 'AKIAJW44TJWRWBUXCDOQ')
AWS_SECRET_ACCESS_KEY = getattr(settings, 'AWS_SECRET_ACCESS_KEY', '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')
AWS_ASSOCIATE_TAG = settings.GET_AWS_ASSOCIATE_TAG(region)

# amazon_api = AmazonAPI(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_ASSOCIATE_TAG, region=region)
# btnose_api = bottlenose.Amazon(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_ASSOCIATE_TAG, Region=region,
#                                Parser=lambda text: BeautifulSoup(text, 'xml'))
btnose_api = bottlenose.Amazon(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_ASSOCIATE_TAG, Region=region)


class AmazonAPI2(object):
    def __init__(self, aws_key, aws_secret, aws_associate_tag, **kwargs):
        if 'region' in kwargs:
            kwargs['Region'] = kwargs['region']
            del kwargs['region']

        if 'Version' not in kwargs:
            kwargs['Version'] = '2013-08-01'

        self.api = bottlenose.Amazon(
            aws_key, aws_secret, aws_associate_tag, **kwargs)
        self.aws_associate_tag = aws_associate_tag
        self.region = kwargs.get('Region', 'US')

    def lookup(self, ResponseGroup="Large", **kwargs):
        response = self.api.ItemLookup(ResponseGroup=ResponseGroup, **kwargs)
        root = objectify.fromstring(response)
        if root.Items.Request.IsValid == 'False':
            code = root.Items.Request.Errors.Error.Code
            msg = root.Items.Request.Errors.Error.Message
            raise LookupException(
                u"Amazon Product Lookup Error: '{0}', '{1}'".format(code, msg))
        if not hasattr(root.Items, 'Item'):
            raise AsinNotFound("ASIN(s) not found: '{0}'".format(
                etree.tostring(root, pretty_print=True)))
        if len(root.Items.Item) > 1:
            return [
                AmazonProduct2(
                    item,
                    self.aws_associate_tag,
                    self,
                    region=self.region) for item in root.Items.Item
            ]
        else:
            return AmazonProduct2(
                root.Items.Item,
                self.aws_associate_tag,
                self,
                region=self.region
            )

class LXMLWrapper2(object):
    def __init__(self, parsed_response):
        self.parsed_response = parsed_response

    def to_string(self):
        return etree.tostring(self.parsed_response, pretty_print=True)

    def _safe_get_element(self, path, root=None):
        """Safe Get Element.

        Get a child element of root (multiple levels deep) failing silently
        if any descendant does not exist.

        :param root:
            Lxml element.
        :param path:
            String path (i.e. 'Items.Item.Offers.Offer').
        :return:
            Element or None.
        """
        elements = path.split('.')
        parent = root if root is not None else self.parsed_response
        for element in elements[:-1]:
            parent = getattr(parent, element, None)
            if parent is None:
                return None
        return getattr(parent, elements[-1], None)

    def _safe_get_element_text(self, path, root=None):
        """Safe get element text.

        Get element as string or None,
        :param root:
            Lxml element.
        :param path:
            String path (i.e. 'Items.Item.Offers.Offer').
        :return:
            String or None.
        """
        element = self._safe_get_element(path, root)
        if element is not None:
            return element.text
        else:
            return None

class AmazonProduct2(LXMLWrapper2):
    def __init__(self, item, aws_associate_tag, api, *args, **kwargs):
        """Initialize an Amazon Product Proxy.

        :param item:
            Lxml Item element.
        """
        super(AmazonProduct2, self).__init__(item)
        self.aws_associate_tag = aws_associate_tag
        self.api = api
        self.parent = None
        self.region = kwargs.get('region', 'US')

    def __str__(self):
        return self.title

    @property
    def asin(self):
        return self._safe_get_element_text('ASIN')

    @property
    def parent_asin(self):
        return self._safe_get_element('ParentASIN')

amazon_api = amazon_api = AmazonAPI2(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_ASSOCIATE_TAG, region=region)