from django.shortcuts import render
from django.conf import settings
from django.views.generic import ListView, DetailView
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator

from lxml import objectify, etree
import bottlenose
from bs4 import BeautifulSoup
from amazon.api import AmazonAPI # library called python-amazon-simple-product-api


region = 'DE'

AWS_ACCESS_KEY_ID = getattr(settings, 'AWS_ACCESS_KEY_ID', 'AKIAJW44TJWRWBUXCDOQ')
AWS_SECRET_ACCESS_KEY = getattr(settings, 'AWS_SECRET_ACCESS_KEY', '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')
AWS_ASSOCIATE_TAG = settings.GET_AWS_ASSOCIATE_TAG(region)

amazon_api = AmazonAPI(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_ASSOCIATE_TAG, region=region)
btnose_api = bottlenose.Amazon(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_ASSOCIATE_TAG, Region=region,
                               Parser=lambda text: BeautifulSoup(text, 'xml'))

btnose_api2 = bottlenose.Amazon(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_ASSOCIATE_TAG, Region=region)

class AmazonProductView(ListView):
    template_name = "amazon_products/view.html"
    
    def get_queryset(self, *args, **kwargs):
        query = self.request.GET.get('q')
        product_search = amazon_api.search_n(20, Keywords=query, SearchIndex='All') # maximum 10 products for testing
        # product_search = self.amazon_api.search(Keywords=query, SearchIndex='All', ResponseGroup='ItemAttributes') # maximum 10 products for testing

        paginator = Paginator(product_search, 20) # 10 products per page
        page = self.request.GET.get('page')
        try:
            amzn_products = paginator.page(page)
        except PageNotAnInteger:
            amzn_products = paginator.page(1) # If page is not an integer, deliver first Page
        except EmptyPage:
            amzn_products = paginator.page(paginator.num_pages) # If page is out of range (e.g. 999), deliver last page of results

        print(amzn_products)

        return amzn_products

class AmazonProductDetailView(DetailView):
    template_name = "amazon_products/detail.html"

    def get_object(self, *args, **kwargs):
        asin = self.kwargs.get('asin')
        product = amazon_api.lookup(ItemId=asin)
        return product

class AmazonProductVariationsDetailView(DetailView):
    template_name = "amazon_products/detail_variations.html"

    def get_object(self, *args, **kwargs):
        asin = self.kwargs.get('asin')
        product = amazon_api.lookup(ItemId=asin)
        product_var_summary = amazon_api.lookup(ItemId=(product.parent_asin or asin), ResponseGroup="VariationSummary")

        # parent_asin = results.find('ParentASIN').string
        parent_asin = product.parent_asin
        results = amazon_api.lookup(ItemId=(product.parent_asin or asin), ResponseGroup="VariationMatrix")

        if results.variation_count is not None:
            product = amazon_api.lookup(ItemId=asin) 
            product.price_range = [float(p)/100 for p in product_var_summary.price_var_range]    
        
            # assumed the attribute names are the same for all variations, check..!
            product.attribute_names = results.var_products[0].attr_names
            attr_count = len(product.attribute_names)
            arr_attr_values = [[] for _ in range(attr_count)]
            for x in results.var_products:
                # print(x.var_asin)
                # print(x.attr_names)
                for index, val in enumerate(x.attr_values):
                    arr_attr_values[index].append(val)

            for index, arr in enumerate(arr_attr_values):
                arr_attr_values[index] = list(dict.fromkeys(arr).keys()) # removing duplicate attribute values
            
            product.attribute_values = arr_attr_values

        return product

        # if parent_asin != asin:
        #     results = btnose_api.ItemLookup(ItemId=parent_asin, ResponseGroup="VariationMatrix")
        #     results_summary = btnose_api.ItemLookup(ItemId=asin, ResponseGroup="VariationSummary")
        #     attribute_names = results.findAll('VariationDimension')
        #     attribute_names = [attr_name.contents[0] for attr_name in attribute_names]
            # print(attribute_names)


    # def get_context_data(self, args, **kwargs):
    #     asin = self.kwargs.get('asin')
    #     product = amazon_api.lookup(ItemId=asin)#, ResponseGroup='VariationSummary') # VariationMatrix
    #     results = btnose_api.ItemLookup(ItemId=asin, ResponseGroup="VariationMatrix")
    #     parent_asin = results.find('ParentASIN').string
    #     if parent_asin != asin:
    #         results = btnose_api.ItemLookup(ItemId=parent_asin, ResponseGroup="VariationMatrix")
    #         attribute_names = results.findAll('VariationDimension')
    #         attribute_names = [attr_name.contents[0] for attr_name in attribute_names]
    #         print(attribute_names)
    #     context = super(AmazonProductVariationsDetailView, self).get_context_data(*args, **kwargs)
    #     context['title'] = 'Test title'
    #     return context

    # def get_queryset(self, *args, **kwargs):
    #     request = self.request
    #     views = None #request.user.objectviewed_set.by_model(ProductOld, model_queryset=False)
    #     return views     


# # function based views
# def current_datetime(request):
#     now = datetime.datetime.now()
#     return render(request, 'current_datetime.html', {'current_date': now})