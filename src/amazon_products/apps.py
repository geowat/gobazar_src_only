from django.apps import AppConfig


class AmazonProductsConfig(AppConfig):
    name = 'amazon_products'
