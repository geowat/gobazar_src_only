from django.conf.urls import url

from .views import (
        AmazonProductView,
        AmazonProductDetailView,
        AmazonProductVariationsDetailView,
        )

urlpatterns = [
    url(r'^$', AmazonProductView.as_view(), name='query'),
    url(r'^(?P<asin>[\w-]+)/$', AmazonProductDetailView.as_view(), name='detail'),
    url(r'^(?P<asin>[\w-]+)/var/$', AmazonProductVariationsDetailView.as_view(), name='detailvar'), # testing variation before integrating into main detail
    # url(r'^(?P<slug>[\w-]+)/$', AmazonProductDetailView.as_view(), name='detail'),
    # url(r'^/detail/$', AmazonProductDetailView.as_view(), name='detail'),
]
